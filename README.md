**Libring - Technical Challenge**

 A Ruby on Rails application that receives parameters by form, calls the Libring Reporting API, and returns the data on the page.

---

## How to use

To generate the report:

1. Must select the start date of gathering.
2. Must select the end date of gathering.
3. Choose the optional parameters of gathering (connection, app, platform and country).
4. The data will return in table.

---

No adicional requirements are required. 
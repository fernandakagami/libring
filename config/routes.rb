Rails.application.routes.draw do

 	root 'report#index'

 	get    '/index',	to: 'report#index'
  	post   '/result',	to: 'report#result'
  	
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

class Report

	require 'net/https'
	require 'uri'

	TOKEN_KEY = 'Token HYCRvSlRAEyRrsHbujvlaoRPf'

	def metric_arrange(metric)		
    	start_date = Date.strptime(metric[:start_date], '%d/%m/%Y').to_s
		end_date = Date.strptime(metric[:end_date], '%d/%m/%Y').to_s
		website = "https://api.libring.com/v2/reporting/get?period=custom_date&start_date=#{start_date}&end_date=#{end_date}&data_type=adnetwork&group_by=date"
		if metric[:connection] == '1'
			website += ",connection"
		end
		if metric[:app] == '1'
			website += ",app"			
		end
		if metric[:platform] == '1'
			website += ",platform"			
		end
		if metric[:country] == '1'
			website += ",country"
		end
		website
	end

	def connect(website)
		uri = URI.parse(website)
		https = Net::HTTP.new(uri.host, uri.port)
		https.use_ssl = true
		req = Net::HTTP::Get.new(uri.request_uri, initheader = {'Content-Type' => 'application/json', 'Authorization' => TOKEN_KEY })
		resp = https.request(req)		
	end

	def collect(metric)
		website = metric_arrange(metric)
		resp = connect(website)
		body = JSON.parse(resp.body)
		connections = body['connections']
		while body['next_page_url'] != ''
			website = body['next_page_url']
			resp = connect(website)
			body = JSON.parse(resp.body)
			connections = connections + body['connections']
		end
		connections		
	end

end

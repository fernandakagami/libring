class ReportController < ApplicationController

	require 'json'

	def index
		
	end	

	def result
		@metric = metric_params
		report = Report.new	
		@connections = report.collect(@metric)
	end

	
	private

	def metric_params
      	params.require(:metric).permit(:start_date, :end_date, :connection, :app, :platform, :country)
    end    

end
